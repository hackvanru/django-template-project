from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Main URLs
    url(r'^', include('apps.main.urls', namespace='main')),

    # Admin URLs
    url(r'^admin/', include(admin.site.urls)),
]
